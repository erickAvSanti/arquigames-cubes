const jQuery = require('jquery');
const THREE = require('three');
const glm = require('gl-matrix');


const func = (function(){

  let instance  = null;
  let capp      = null;

  let scene = null;
  let camera = null;
  let gridHelper = null;
  let rollOverGeo = null;
  let rollOverMaterial = null;
  let rollOverMesh = null;

  let objects = [];
  let mouse2D = new THREE.Vector2();
  let raycaster = new THREE.Raycaster();
  let isShiftDown = false;
  let isCtrlDown = false;
  let theta = 45 * 0.5;
  let plane = null;
  let intersector = null;
  let step = 10;
  let half_step = 5;
  let step_total = 10;
  let tmpVec = new THREE.Vector3();
  let normalMatrix = new THREE.Matrix3();
  let voxelPosition = new THREE.Vector3();

  let cubeGeo = null;
  let cubeMaterial = null;

  let max_cubes = 3000;
  let pendingJson = null;


  const dev_mode = process.env.NODE_ENV === 'development';

  class VoxelPainter{
    constructor(_capp){
      capp = _capp;
    }
    init(){
      scene = capp.getScene();
      camera = capp.getCamera();
      gridHelper = new THREE.GridHelper( step_total*step, step_total );
      scene.add( gridHelper );

      rollOverGeo = new THREE.BoxBufferGeometry( step, step, step );
      rollOverMaterial = new THREE.MeshBasicMaterial( { color: 0xff0000, opacity: 0.5, transparent: true } );
      rollOverMesh = new THREE.Mesh( rollOverGeo, rollOverMaterial );
      scene.add( rollOverMesh );

      var geometry = new THREE.PlaneBufferGeometry( step_total*step, step_total*step );
      geometry.rotateX( - Math.PI / 2 );
      plane = new THREE.Mesh( geometry, new THREE.MeshBasicMaterial( { visible: false } ) );
      scene.add( plane );
      objects.push(plane);

      jQuery(document).mousemove(function(evt){
        instance.onDocumentMouseMove(evt);
      });
      jQuery(document).mousedown(function(evt){
        instance.onDocumentMouseDown(evt);
      });
      jQuery(document).keydown(function(evt){
        instance.onDocumentKeyDown(evt);
      });
      jQuery(document).keyup(function(evt){
        instance.onDocumentKeyUp(evt);
      });

      let loader = new THREE.TextureLoader();
      let _this = this;

      loader.load( 
        require('@/assets/images/texture.png'), 
        function ( texture ) { 
          cubeGeo = new THREE.BoxGeometry( step, step, step );
          cubeMaterial = new THREE.MeshLambertMaterial( { color: 0xfeb74c, flatShading: true, map: texture,transparent:true,opacity:1 } ); 
          if(dev_mode){
            console.log("texture",texture);
            console.log("cubeMaterial",cubeMaterial);
          }
          if( pendingJson ){
            instance.addVoxelFromJson( pendingJson );
          }
        }, 
        function ( xhr ) {
          if(dev_mode)console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );
        }, 
        function ( xhr ) {
          if(dev_mode)console.log( 'An error happened' );
        }
      ); 

    }
    loop(){
      if ( isCtrlDown ) {

        theta += mouse2D.x * 1.5;

      }
      raycaster.setFromCamera( mouse2D, camera );
      //raycaster = projector.pickingRay( mouse2D.clone(), camera );

      var intersects = raycaster.intersectObjects( objects );
      if ( intersects.length > 0 ) {

        intersector = instance.getRealIntersector( intersects );

        if ( intersector ) {

          instance.setVoxelPosition( intersector );
          rollOverMesh.position.copy( voxelPosition );

        }

      }
    }
    onDocumentMouseMove(evt){
      mouse2D.x = 0;
      mouse2D.y = 0;
    }
    onDocumentMouseDown(evt){
      var intersects = raycaster.intersectObjects( objects );

      if ( intersects.length > 0 ) {

        intersector = instance.getRealIntersector( intersects );

        // delete cube

        if ( isShiftDown ) {

          if ( intersector.object != plane ) {
            scene.remove( intersector.object );
            objects.splice( objects.indexOf( intersector.object ), 1 );
            let kk = instance.three_vector3_str(intersector.object.position);
            if(kk in capp.objects_set)delete capp.objects_set[kk];
          }

        // create cube

        }else{

          if(
              capp.hasLocked===true &&
              cubeMaterial
          ){
            intersector = instance.getRealIntersector( intersects );
            instance.setVoxelPosition( intersector );
            instance.addVoxel();
          } 
        } 
      }
    }
    addVoxel(color_hex = null){
      if(objects.length > max_cubes)return;
      var cubeMaterialClone = cubeMaterial.clone();
      cubeMaterialClone.color = new THREE.Color(color_hex ? color_hex : capp.selected_color);
      var voxel = new THREE.Mesh( cubeGeo, cubeMaterialClone );
      voxel.position.copy( voxelPosition );
      voxel.matrixAutoUpdate = false;
      voxel.updateMatrix();
      voxel.receiveShadow=true;
      voxel.castShadow=true;
      scene.add( voxel );
      objects.push( voxel );
      capp.objects_set[instance.three_vector3_str(voxel.position)] = {
        color:(color_hex ? color_hex : capp.selected_color),
      };
    }
    three_vector3_str(v3){
      return v3.x+'_'+v3.y+'_'+v3.z;
    }
    onDocumentKeyDown(evt){
      switch ( event.keyCode ) {
        case 16: isShiftDown = true; break;
        case 17: isCtrlDown = true; break;
      }
    }
    onDocumentKeyUp(evt){
      switch ( event.keyCode ) {
        case 16: isShiftDown = false; break;
        case 17: isCtrlDown = false; break;
      }
    }
    getRealIntersector( intersects ){
      for( let i = 0; i < intersects.length; i++ ) {
        intersector = intersects[ i ];
        if ( intersector.object != rollOverMesh ) {
          return intersector;
        }
      }
      return null;
    }
    setVoxelPosition( intersector ){  
      normalMatrix.getNormalMatrix( intersector.object.matrixWorld );
      tmpVec.copy( intersector.face.normal );
      tmpVec.applyMatrix3( normalMatrix ).normalize();
      voxelPosition.addVectors( intersector.point, tmpVec );
      voxelPosition.divideScalar( step ).floor().multiplyScalar( step ).addScalar( half_step );

    }
    putObjectsFromJson(json){
      if(cubeMaterial){
        instance.addVoxelFromJson(json);
      }else{
        pendingJson = json;
      }
    }
    addVoxelFromJson(json){
      let idx;
      objects.map(function(obj){
        scene.remove( obj );
      });
      objects.splice( 0, objects.length );
      objects.push( plane );
      if(capp.objects_set){
        delete capp.objects_set;
        capp.objects_set = {};
      }
      for(idx in json){
        let pos = idx.split("_");
        if(pos.length==3){
          let data = json[idx];
          try{
            voxelPosition = new THREE.Vector3(
              window.parseInt(pos[0]),
              window.parseInt(pos[1]),
              window.parseInt(pos[2])
            );
            instance.addVoxel(data.color);
          }catch(e){
            if(dev_mode)console.log(e.stack);
          }
        }
      }
    }
  }

  return {
    getInstance(obj){
      if(!instance || dev_mode || true){
        instance = new VoxelPainter(obj);
        return instance;
      }else{
        return null;
      }
    }
  };
})();

export default func;