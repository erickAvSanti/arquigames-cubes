const jQuery = require('jquery');
const THREE = require('three');
const glm = require('gl-matrix');


const func = (function(){

  let instance  = null;
  let capp      = null;


  let origin    = new THREE.Vector3(0,0,0);

  let lookAtPoint = glm.vec3.create();
  let upVector    = glm.vec3.create();
  let eyePoint    = glm.vec3.create();

  let velocityI_W = 0;
  let velocityF_W = 0;
  let acceleration_W = 0.3;
  let velocityI_S = 0;
  let velocityF_S = 0;
  let acceleration_S = 0.3;
  let velocityI_Q = 0;
  let velocityF_Q = 0;
  let acceleration_Q = 0.1;
  let velocityI_Z = 0;
  let velocityF_Z = 0;
  let acceleration_Z = 0.1;
  let distance_W = 0;
  let distance_S = 0;
  let distance_Z = 0;
  let distance_Q = 0;
  let currentTime = Date.now();

  let velocity_constant = 8;
  let use_velocity_constant = true;
  const dev_mode = process.env.NODE_ENV === 'development';

  class CameraUtils{
    constructor(_capp){
      capp = _capp;
      this.ready = false;
      currentTime = Date.now();
      lookAtPoint = glm.vec3.create();
      upVector    = glm.vec3.create();
      eyePoint    = glm.vec3.create();
      origin    = new THREE.Vector3(0,0,0);
    }
    rotateViewAround(deltaAngle, axis) {
      if(!this.ready)return;
      let frontDirection = glm.vec3.create();
      glm.vec3.subtract(frontDirection, lookAtPoint, eyePoint);
      glm.vec3.normalize(frontDirection , frontDirection);
      let q = glm.quat.create();
      glm.quat.setAxisAngle(q, axis, deltaAngle);
      glm.vec3.transformQuat(frontDirection, frontDirection, q);
      glm.vec3.copy(lookAtPoint,eyePoint);
      glm.vec3.add(lookAtPoint, lookAtPoint, frontDirection);   
    }
    rotateView(x, y) {  
      if(!this.ready)return;
      let frontDirection = glm.vec3.create();
      let strafeDirection = glm.vec3.create();
      glm.vec3.subtract(frontDirection, lookAtPoint, eyePoint);
      glm.vec3.normalize(frontDirection,frontDirection);
      glm.vec3.cross(strafeDirection, frontDirection, upVector);
      glm.vec3.normalize(strafeDirection,strafeDirection);
      this.rotateViewAround(-x/360.0, upVector); 
      this.rotateViewAround(-y/360.0, strafeDirection); 
        
    }
    moveView(w,s,a,d,q,e,z,c, dt) {  
      if(!this.ready)return;
      let frontDirection = glm.vec3.create();
      let strafeDirection = glm.vec3.create();
      let frontParalelDirection = glm.vec3.create();
      glm.vec3.set(frontParalelDirection,0,1,0);
      let strafeParalelDirection = glm.vec3.create();
      glm.vec3.subtract(frontDirection, lookAtPoint, eyePoint);
      glm.vec3.normalize(frontDirection, frontDirection);
      glm.vec3.cross(strafeDirection, frontDirection, upVector);
      glm.vec3.normalize(strafeDirection, strafeDirection);
      glm.vec3.copy(strafeParalelDirection, strafeDirection); 

      let forwardScale = 0.0;
      let strafeScale = 0.0;
      let topScale = 0.0;
      let frontParalelScale = 0.0;


      if (w) {
        //forwardScale += 1.0; 
        frontParalelScale += 1.0;
      }
      if (s) {
        //forwardScale -= 1.0; 
        frontParalelScale -= 1.0;
        
      }
      if (a) {
        strafeScale -= 1.0;
      }
      if (d) {
        strafeScale += 1.0;
      } 
      if (q) {
        topScale += 1.0;
      }
      if (e) {
        topScale -= 1.0;
      }
      if (z) {
        frontParalelScale += 1.0;
      }
      if (c) {
        frontParalelScale -= 1.0;
      } 
      if(forwardScale==0){
        velocityI_W=0;
        velocityF_W=0;
        if(acceleration_W<0){
          acceleration_W = - acceleration_W;
        }
      }
      if(strafeScale==0){
        velocityI_S=0;
        velocityF_S=0;
        if(acceleration_S<0){
          acceleration_S = - acceleration_S;
        }
      }
      if(topScale==0){
        velocityI_Q=0;
        velocityF_Q=0;
        if(acceleration_Q<0){
          acceleration_Q = - acceleration_Q;
        }
      }
      if(frontParalelScale==0){
        velocityI_Z=0;
        velocityF_Z=0;
        if(acceleration_Z<0){
          acceleration_Z = - acceleration_Z;
        }
      }


      let timeVariation = 4.0 * dt; 
      /*
      forwardScale *= timeVariation;
      strafeScale *= timeVariation;
      */
      if(capp.hasLocked===true && !use_velocity_constant){
        /*calculo de la ditancia a avanzar utilizando las velocidades y la aceleracion*/
        velocityI_W  = velocityF_W;
        velocityF_W  = velocityI_W + acceleration_W*timeVariation;
        distance_W   = velocityI_W*timeVariation + 0.5*acceleration_W*timeVariation*timeVariation;

        forwardScale *= distance_W;

        /*********************************************************************************************/

        velocityI_S  = velocityF_S;
        velocityF_S  = velocityI_S + acceleration_S*timeVariation;
        distance_S   = velocityI_S*timeVariation + 0.5*acceleration_S*timeVariation*timeVariation;

        strafeScale *= distance_S;

        /*********************************************************************************************/

        velocityI_Q  = velocityF_Q;
        velocityF_Q  = velocityI_Q + acceleration_Q*timeVariation;
        distance_Q   = velocityI_Q*timeVariation + 0.5*acceleration_Q*timeVariation*timeVariation;

        topScale *= distance_Q;

        /*********************************************************************************************/

        velocityI_Z  = velocityF_Z;
        velocityF_Z  = velocityI_Z + acceleration_Z*timeVariation;
        distance_Z   = velocityI_Z*timeVariation + 0.5*acceleration_Z*timeVariation*timeVariation;

        frontParalelScale *= distance_Z;
      }else{
        forwardScale      *= timeVariation * velocity_constant;
        strafeScale       *= timeVariation * velocity_constant;
        topScale          *= timeVariation * velocity_constant;
        frontParalelScale *= timeVariation * velocity_constant;
      }
      /*console.log("forwardScale: "+forwardScale);
      console.log("strafeScale: "+strafeScale);
      console.log("topScale: "+topScale);
      console.log("frontParalelScale: "+frontParalelScale);*/
      


      glm.vec3.scale(frontDirection, frontDirection, forwardScale);
      glm.vec3.scale(strafeDirection, strafeDirection, strafeScale);

      glm.vec3.add(eyePoint, eyePoint, frontDirection);
      glm.vec3.add(eyePoint, eyePoint, strafeDirection);
      glm.vec3.add(lookAtPoint, lookAtPoint, frontDirection);
      glm.vec3.add(lookAtPoint, lookAtPoint, strafeDirection);

        
      eyePoint[1] += topScale;
      lookAtPoint[1] += topScale;





      let q1 = glm.quat.create();
      glm.quat.setAxisAngle(q1, strafeParalelDirection, Math.PI*180/360);
      glm.vec3.transformQuat(frontParalelDirection, frontParalelDirection, q1);
      glm.vec3.scale(frontParalelDirection, frontParalelDirection, frontParalelScale); 

      glm.vec3.add(eyePoint, eyePoint, frontParalelDirection); 
      glm.vec3.add(lookAtPoint, lookAtPoint, frontParalelDirection); 
    }
    setEyePoint(three_vector3){
      eyePoint[0] = three_vector3.x;
      eyePoint[1] = three_vector3.y;
      eyePoint[2] = three_vector3.z;
    }
    setUpVector(three_vector3){
      upVector[0] = three_vector3.x;
      upVector[1] = three_vector3.y;
      upVector[2] = three_vector3.z;
    }
    getLookAtPoint(three_version = false){
      if(three_version){
        return new THREE.Vector3(lookAtPoint[0],lookAtPoint[1],lookAtPoint[2]);
      }else{
        return lookAtPoint;
      }
    }
    getEyePoint(three_version = false){
      if(!eyePoint || eyePoint.length<3)console.log("eyePoint empty",eyePoint);
      if(three_version){
        return new THREE.Vector3(eyePoint[0],eyePoint[1],eyePoint[2]);
      }else{
        return eyePoint;
      }
    }
  }

  return {
    getInstance(obj){
      if(!instance || dev_mode || true){
        instance = new CameraUtils(obj);
        return instance;
      }else{
        return null;
      }
    }
  };
})();

export default func;