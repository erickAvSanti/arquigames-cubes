const THREE = require('three');
const jQuery = require('jquery');
import CameraUtils from "@/assets/js/cameraUtils.js";
import VoxelPainter from "@/assets/js/voxelPainter.js";

function init(_vue){
	var instance = null;
	var scene = new THREE.Scene();
	var camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 0.1, 10000 );
	camera.position.set( 50, 80, 130 );
	camera.lookAt(new THREE.Vector3(0,0,0));
	var renderer = new THREE.WebGLRenderer({antialias: true});
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth, window.innerHeight );
	var element = renderer.domElement;
	jQuery("#canvas_container").html(element);

	var cube,plane;
	var code = Math.random();
	var loopers = [];

	let cameraUtils = null;
	let voxelPainter = null;
	let debug = false;

	let duration = 5000;
  let currentTime = Date.now();

  let runAnimation = true;

  let key = {
    frontParalelKey : false,
    backParalelKey : false,
    leftKey : false,
    rightKey : false,
    topKey : false,
    bottomtKey : false,
    frontParalelKey : false,
    backParalelKey : false,
  };

  let rotView = {x:0,y:0};

	const dev_mode = process.env.NODE_ENV === 'development';

	let _keydown_callback = null;
	let _keyup_callback = null;
	let _resize_callback = null;

	class CApp{
		constructor(_vue){
			cameraUtils = CameraUtils.getInstance(this);
			voxelPainter = VoxelPainter.getInstance(this);
			this.hasLocked = false;
			this.selected_color = "#ff0000";
			this.vue_container = _vue;
			this.objects_set = {};
		}
		reset(){
			currentTime = Date.now();
		}
		setBackground(opt,value){
			if( opt=='b' )scene.background = new THREE.Color(value);
		}
		init() {
			var geometry, material;
			geometry = new THREE.BoxGeometry( 1, 1, 1 );
			material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
			if(debug){
				cube = new THREE.Mesh( geometry, material );
				scene.add( cube );
			} 
			var ambientLight = new THREE.AmbientLight( 0x606060 );
			scene.add( ambientLight );
			var directionalLight = new THREE.DirectionalLight( 0xffffff );
			directionalLight.position.set( 1, 0.75, 0.5 ).normalize();
			scene.add( directionalLight );

			camera.position.z = 5;
			cameraUtils.setEyePoint(camera.position);
			cameraUtils.setUpVector(camera.up);
			cameraUtils.ready = true;

			if(!_keydown_callback){
				_keydown_callback = function(evt){
					let kk = evt.key.toLowerCase();
					if( evt.code=='KeyW' || evt.keyCode==87 || evt.which==87 || evt.key=='w' )key.backParalelKey 	= true;
					if( evt.code=='KeyS' || evt.keyCode==83 || evt.which==83 || evt.key=='s' )key.frontParalelKey	= true;
					if( evt.code=='KeyA' || evt.keyCode==65 || evt.which==65 || evt.key=='a' )key.leftKey 				= true;
					if( evt.code=='KeyD' || evt.keyCode==68 || evt.which==68 || evt.key=='d' )key.rightKey 				= true;
					if( evt.code=='KeyQ' || evt.keyCode==81 || evt.which==81 || evt.key=='q' )key.topKey 					= true;
					if( evt.code=='KeyE' || evt.keyCode==69 || evt.which==69 || evt.key=='e' )key.bottomtKey 			= true;
				};
				jQuery(window).keydown(_keydown_callback);
			}
			if(!_keyup_callback){
				_keyup_callback = function(evt){
					if( evt.code=='KeyW' || evt.keyCode==87 || evt.which==87 || evt.key=='w' )key.backParalelKey 	= false;
					if( evt.code=='KeyS' || evt.keyCode==83 || evt.which==83 || evt.key=='s' )key.frontParalelKey	= false;
					if( evt.code=='KeyA' || evt.keyCode==65 || evt.which==65 || evt.key=='a' )key.leftKey 				= false;
					if( evt.code=='KeyD' || evt.keyCode==68 || evt.which==68 || evt.key=='d' )key.rightKey 				= false;
					if( evt.code=='KeyQ' || evt.keyCode==81 || evt.which==81 || evt.key=='q' )key.topKey 					= false;
					if( evt.code=='KeyE' || evt.keyCode==69 || evt.which==69 || evt.key=='e' )key.bottomtKey 			= false;
	        if( ( evt.code=='KeyB' || evt.keyCode==66 || evt.which==66 || evt.key=='b' ) && instance.vue_container && instance.vue_container.modals && !instance.vue_container.modals.reg)runAnimation = !runAnimation;
				};
				jQuery(window).keyup(_keyup_callback);
			}

			if(voxelPainter){
				voxelPainter.init();
				this.addLooper(voxelPainter.loop);
			}

		}
		addEvents(){
			jQuery(window).resize(function(evt){instance.resized = true;});
		}
		addLooper(_looper){
			if(!loopers.includes(_looper))loopers.push(_looper);
		}
		getCamera(){
			return camera;
		}
		getScene(){
			return scene;
		}
		getRenderer(){
			return renderer;
		}
		rotateView(x,y){
			rotView.x = x;
			rotView.y = y;
		}
		getElement(){
			return element;
		}
		putObjects(json){
			voxelPainter.putObjectsFromJson(json);
		}
	};


	function animate() {
		window.req_anim = window.requestAnimationFrame( animate );
	  let now = Date.now();
	  let deltatime = now - currentTime;
	  currentTime = now;
	  let fraction = deltatime / duration;
		if( runAnimation ){

			if(debug){
				cube.rotation.x += 0.01;
				cube.rotation.y += 0.01;
			}
			for(var idx in loopers){
				loopers[idx]();
			}  

			if(cameraUtils){
				if(rotView.x != 0 || rotView.y != 0){
					cameraUtils.rotateView(rotView.x,rotView.y);
					rotView.x = 0;
					rotView.y = 0;
				}
				if(instance.vue_container.share_mode || (instance.vue_container && !instance.vue_container.modals.reg)){
			 		cameraUtils.moveView(
			      key.frontParalelKey,
			      key.backParalelKey,
			      key.leftKey,
			      key.rightKey,
			      key.topKey,
			      key.bottomtKey,
			      key.frontParalelKey,
			      key.backParalelKey,
			      fraction*10
			    );
				}
				camera.position.copy(cameraUtils.getEyePoint(true));
				camera.lookAt(cameraUtils.getLookAtPoint(true));
			}
			renderer.render( scene, camera );
		}
		if(instance.resized){
			resize();
			instance.resized = false;
		}
	}
	function resize(){
		camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );
	}

	if(!instance || dev_mode){
		instance = new CApp(_vue);
		instance.init();
		animate();
		instance.addEvents();
		return instance;
	}
	return null;

}
export {init};