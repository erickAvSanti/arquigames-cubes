import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//import './registerServiceWorker'

require('dotenv').config()

import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)

import { library } from '@fortawesome/fontawesome-svg-core'
import { 
	faTimes,
	faEllipsisV,
	faPlus,
	faEdit,
	faSave,
	faLink,
	faMousePointer,
	faExternalLinkSquareAlt,
	faShareAlt,
	faPalette,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import i18n from './i18n'

library.add(
	faTimes,
	faEllipsisV,
	faPlus,
	faEdit,
	faSave,
	faLink,
	faMousePointer,
	faExternalLinkSquareAlt,
	faShareAlt,
	faPalette,
)

Vue.component('font-awesome-icon', FontAwesomeIcon)


Vue.config.productionTip = false

new Vue({
    router,
    store,
    i18n,
    render: h => h(App),
}).$mount('#app')
